namespace TDDMoney
{
    public class Money
    {
        protected int _amount;
        private readonly string _currency;

        public Money(int amount, string currency)
        {
            _amount = amount;
            _currency = currency;
        }

        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        public string Currency { get{return _currency;}  }

        public override bool Equals(object objToCompare)
        {
            var money = objToCompare as Money;
            return this.Amount == money.Amount && this.Currency == money.Currency;
        }

        public static Money Dollar(int amt)
        {
            return new Dollar(amt, "USD");
        }

        public static Money Franc(int amt)
        {
            return new Franc(amt, "CHF");
        }

        public Money Times(int multiplier)
        {
            return new Money(_amount * multiplier, Currency);
        }

        public override string ToString()
        {
            return Amount + " " + Currency;
        }
    }
}