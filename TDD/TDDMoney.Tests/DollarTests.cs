﻿using NUnit.Framework;

namespace TDDMoney.Tests
{
    [TestFixture]
    public class DollarTests
    {
        [Test]
        public void Test_Multiplication()
        {
            var five = Money.Dollar(5);

            var product2 = five.Times(2);
            Assert.AreEqual(new Dollar(10, "USD"), product2);
            Assert.AreEqual(Money.Dollar(10), five.Times(2));

            var anotherProduct3 = five.Times(3);
            Assert.AreEqual(new Dollar(15, "USD"), anotherProduct3);
            Assert.AreEqual(Money.Dollar(15), five.Times(3));
        }

        [Test]
        public void Test_Equality()
        {
            //Arrange
            var dollarfive_a = new Dollar(5, "USD");
            var dollarfive_b = new Dollar(5, "USD");
            var francFive_a = new Franc(5, "CHF");
            var francFive_b = new Franc(5, "CHF");

            //Act

            //Assert
            Assert.AreEqual(dollarfive_a, dollarfive_b);
            Assert.AreEqual(francFive_a, francFive_b);
            Assert.AreNotEqual(francFive_a, dollarfive_b);
            Assert.AreNotEqual(francFive_b, dollarfive_b);
            Assert.AreEqual(Money.Dollar(5), Money.Dollar(5));
            
        }

        [Test]
        public void Test_InEquality()
        {
            //Arrange
            var five = new Dollar(5, "USD");
            var six = new Dollar(6, "USD");

            //Act

            //Assert
            Assert.AreNotEqual(five, six);
            Assert.AreNotEqual(Money.Dollar(5), Money.Dollar(6));
        }

        [Test]
        public void Test_FrancMultiplication()
        {
            Franc five = new Franc(5, "CHF");
            Assert.AreEqual(new Franc(10, "CHF"), five.Times(2));
            Assert.AreEqual(new Franc(15, "CHF"), five.Times(3));
            Assert.AreEqual(Money.Franc(10), five.Times(2));
        }

        [Test]
        public void Test_Currency()
        {
            Assert.AreEqual("USD", Money.Dollar(1).Currency);
            Assert.AreEqual("CHF", Money.Franc(1).Currency);
        }

        [Test]
        public void Test_DifferenClassEquality()
        {
            Assert.AreEqual(new Money(10, "CHF"), new Franc(10, "CHF") );
        }
       
    }
}
